
# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

export IPYTHON=1
export VISUAL=vim
export EDITOR="$VISUAL"

alias rm='rm -i'
alias mv='mv -i'

export LANGUAGE=en_US.UTF-8

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8


export mos_code=/opt/cisco/mos/node/lib/node_modules

if [ $(id -u) -eq 0 ]; then
	PS1='\[\033[01;36m\]\u@\h\[\033[00m\]:\[\033[01;36m\]\W\[\033[00m\]# '
else
	PS1='\[\033[01;36m\]\u@\h\[\033[00m\]:\[\033[01;36m\]\W\[\033[00m\]$ '
fi

if [ -f "$HOME/.bashrc_local" ]; then
	source $HOME/.bashrc_local
fi

source ~/.git-completion.bash
source ~/.docker-completion
source ~/.docker-compose

alias sdocker='docker -H :4000'
alias tmuxa='tmux attach -t'
alias tmuxn='tmux new -s'
export PATH=~/go/bin:/home/keysaim/proj/qpark/prog/shinfra/tools/:$PATH
export GOPATH=~/go
