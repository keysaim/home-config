#!/bin/sh

if [ ! -d .vim/bundle/Vundle.vim ]; then
    git clone https://github.com/VundleVim/Vundle.vim.git .vim/bundle/Vundle.vim
fi

if [ ! -d .tmux/plugins/tpm ]; then
    git clone https://github.com/tmux-plugins/tpm .tmux/plugins/tpm
fi

yum -y install gcc make ncurses-devel ctags cscope

mkdir -p tmp/
cd tmp

# Install libevent

if [ ! -f libevent-2.1.8-stable.tar.gz ]; then
    curl -v -L -o libevent-2.1.8-stable.tar.gz https://github.com/libevent/libevent/releases/download/release-2.1.8-stable/libevent-2.1.8-stable.tar.gz
    if [ $? -ne 0 ]; then
        echo "Failed to download libevent"
        exit 1
    fi
fi
if [ ! -f /usr/local/lib/libevent.a ]; then
    tar xzf libevent-2.1.8-stable.tar.gz
    cd libevent-2.1.8-stable
    if [ $? -ne 0 ]; then
        echo "Failed to decompress libevent"
        exit 1
    fi
    ./configure --prefix=/usr/local && make && make install
    cd ..
fi

# Install tmux

if [ ! -f tmux-2.6.tar.gz ]; then
    curl -v -L -o tmux-2.6.tar.gz https://github.com/tmux/tmux/releases/download/2.6/tmux-2.6.tar.gz
fi

if [ ! -f /usr/local/bin/tmux ]; then
    tar xzf tmux-2.6.tar.gz
    cd tmux-2.6
    LDFLAGS="-L/usr/local/lib -Wl,-rpath=/usr/local/lib" ./configure --prefix=/usr/local && make && make install
fi

echo "System Init Successfully!"

